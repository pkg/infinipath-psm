Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/

Files: *
Copyright: 2013, 2014, Intel Corporation. / 2006-2011, / 2003-2006, PathScale, Inc.
 2007, Cisco, Inc.
License: BSD-2-clause and/or GPL-2 or GPL-2

Files: Makefile
 README
Copyright: 2013, 2014, Intel Corporation. / 2006-2011, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: buildflags.mak
Copyright: 2012, / 2006-2008, QLogic Corporation. / 2003-2005, PathScale, Inc.
License: GPL-2

Files: debian/*
Copyright: 2015-2017 Q-Leap Networks GmbH <info@q-leap.de>
            2016 Ana Beatriz Guerrero Lopez <ana@debian.org>
License: GPL-2+

Files: doc/*
Copyright: 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: include/*
Copyright: 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: include/ipath_common.h
 include/ipath_intf.h
 include/ipath_user.h
Copyright: 2013, / 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: include/ipath_queue.h
Copyright: 1991, 1993, The Regents of the University of California.
License: BSD-4-Clause-UC

Files: include/valgrind/*
Copyright: 2000-2007, Julian Seward.
License: GPL-2

Files: infinipath-psm.spec.in
 intel-mic-psm.spec.in
Copyright: 2012, 2013, / 2010
License: GPL-2

Files: intel-mic-psm-card.spec.in
Copyright: no-info-found
License: GPL-2

Files: ipath/*
Copyright: 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: ipath/Makefile
Copyright: 2012, / 2006-2010, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: ipath/ipath_debug.c
 ipath/ipath_proto.c
 ipath/ipath_protomic.c
 ipath/ipath_service.c
 ipath/ipath_syslog.c
 ipath/ipath_utils.c
Copyright: 2013, / 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: libuuid/*
Copyright: no-info-found
License: BSD-3-clause

Files: libuuid/Makefile
 libuuid/psm_uuid.c
 libuuid/psm_uuid.h
Copyright: 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: libuuid/clear.c
 libuuid/compare.c
 libuuid/copy.c
 libuuid/gen_uuid.c
 libuuid/isnull.c
 libuuid/pack.c
 libuuid/parse.c
 libuuid/tst_uuid.c
 libuuid/unpack.c
 libuuid/unparse.c
 libuuid/uuid.h
 libuuid/uuidP.h
 libuuid/uuid_time.c
Copyright: 1996-1999, Theodore Tso.
License: BSD-3-clause

Files: mpspawn/*
Copyright: 2013, / 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: psm.c
 psm.h
 psm_am.c
 psm_context.c
 psm_diags.c
 psm_ep.c
 psm_ep.h
 psm_mq.c
 psm_mq.h
 psm_mq_internal.h
 psm_mq_recv.c
 psm_mq_utils.c
 psm_stats.c
 psm_stats.h
 psm_utils.c
 psm_utils.h
 ptl.h
Copyright: 2013, / 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: psm_am.h
 psm_am_internal.h
 psm_context.h
 psm_ep_connect.c
 psm_error.c
 psm_error.h
 psm_help.h
 psm_lock.h
 psm_memcpy.c
 psm_mpool.c
 psm_mpool.h
 psm_noship.h
 psm_timer.c
 psm_timer.h
 psm_user.h
Copyright: 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: psmd/*
Copyright: 2012, 2017 / 2005-2006
License: BSD-3-clause and/or GPL-2

Files: psmd/psmd.c
Copyright: 2013, / 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: ptl_am/*
Copyright: 2013, / 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: ptl_am/Makefile
 ptl_am/kcopyrw.h
Copyright: 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: ptl_am/knemrw.h
 ptl_am/knemrwu.c
Copyright: no-info-found
License: GPL-2

Files: ptl_am/scifrw.h
 ptl_am/scifrwu.c
Copyright: 2012, 2013, / 2010
License: GPL-2

Files: ptl_ips/*
Copyright: 2013, / 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: ptl_ips/Makefile
 ptl_ips/ips_epstate.c
 ptl_ips/ips_epstate.h
 ptl_ips/ips_proto_internal.h
 ptl_ips/ips_recvq.c
 ptl_ips/ips_recvq.h
 ptl_ips/ips_spio.h
 ptl_ips/ips_stats.h
 ptl_ips/ips_subcontext.c
 ptl_ips/ips_subcontext.h
 ptl_ips/ips_tid.c
 ptl_ips/ips_tid.h
 ptl_ips/ips_writehdrq.c
 ptl_ips/ipserror.c
 ptl_ips/ipserror.h
 ptl_ips/ptl_fwd.h
 ptl_ips/ptl_ips.h
 ptl_ips/ptl_rcvthread.c
Copyright: 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: ptl_ips/ips_crc32.c
Copyright: 1995-2005, Jean-loup Gailly and Mark Adler
License: Zlib

Files: ptl_ips/ips_path_rec.h
Copyright: no-info-found
License: GPL-2

Files: ptl_self/*
Copyright: 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2

Files: ptl_self/ptl.c
Copyright: 2013, / 2006-2012, / 2003-2006, PathScale, Inc.
License: GPL-2
